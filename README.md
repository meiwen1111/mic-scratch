# xiaou-open

#### 介绍
小幽编程，免费的开源 - 基础社区项目。
搭建请在网站底部设置‘小幽编程’，链接到小幽官网。

#### 软件安装说明
https://www.scratchgui.com/open/doc

#### 小幽官网
https://www.scratchgui.com
更多功能请访问小幽官网